var express = require('express');
var app = express();
var path = require('path');
var MongoClient = require('mongodb').MongoClient

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static('assets'))

function getOne(cb) {
    MongoClient.connect('mongodb+srv://User2:Jordan20180427@cluster0-mt0lw.mongodb.net/test', function(err, client) {
      const db = client.db('test');
      const collection = db.collection('local_images_fourth');
      
      collection.find({}).toArray(cb);
    
    client.close();
  }); 
}



app.get('/', function(req, res) {
  getOne(function(err, result){
    if(err){
      return console.log(err)
    }
    res.render('quiz', { images: result })
  });

})

app.post('/', function(req, res) {
 res.redirect('/result')
})

app.post('/loose', function(req, res) {
  res.redirect('/looser')
})

app.get('/looser', function(req, res){
   res.send('<h1> loser! </h1>')
})

app.get('/result', function(req, res) {
  res.send('<h1> winner! </h1>')
})

app.listen(9000, () => console.log('Example app listening on port 9000!'));




// var uuid = require('uuid');
// var kafka = require('kafka-node');

// var Client = kafka.Client;

// const client = new Client('localhost:2181');

// const producer = new kafka.HighLevelProducer(client);
// producer.on("ready", function() {
//     console.log("Kafka Producer is connected and ready.");
// });
 
// producer.on("error", function(error) {
//     console.error(error);
// });

// const KafkaService = {
//   sendRecord: ({ type, userId, sessionId, data }, callback = () => {}) => {
//       if (!userId) {
//           return callback(new Error(`A userId must be provided.`));
//       }

//       const event = {
//           id: uuid.v4(),
//           timestamp: Date.now(),
//           userId: userId,
//           sessionId: sessionId,
//           type: type,
//           data: data
//       };

//       const buffer = new Buffer.from(JSON.stringify(event));

//       // Create a new payload
//       const record = [
//           {
//               topic: "users",
//               messages: buffer,
//               attributes: 1 /* Use GZip compression for the payload */
//           }
//       ];

//       producer.send(record, callback);
//   }
// };

// app.get('/', function(req, res) {
  
// })

// KafkaService.sendRecord({type: 'BOOLEAN', userId: 1, sessionId: 1, data: 'TRUE'}, function(err){
//   console.log(err)
// })

